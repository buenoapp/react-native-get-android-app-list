import { NativeModules } from 'react-native';

type GetAndroidAppListType = {
  getInstalledApps(): Promise<Object>;
};

const { GetAndroidAppList } = NativeModules;

export default GetAndroidAppList as GetAndroidAppListType;
