package com.reactnativegetandroidapplist;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.module.annotations.ReactModule;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import java.text.SimpleDateFormat;

@ReactModule(name = GetAndroidAppListModule.NAME)
public class GetAndroidAppListModule extends ReactContextBaseJavaModule {
    public static final String NAME = "GetAndroidAppList";

    private ReactContext nReactContext;

    public GetAndroidAppListModule(ReactApplicationContext reactContext) {
        super(reactContext);
        nReactContext = reactContext;
    }

    @Override
    @NonNull
    public String getName() {
        return NAME;
    }


    // Example method
    // See https://reactnative.dev/docs/native-modules-android
    @ReactMethod
  public void getInstalledApps(Promise promise) {
    int flags = PackageManager.GET_META_DATA |
            PackageManager.GET_SHARED_LIBRARY_FILES |
            PackageManager.GET_UNINSTALLED_PACKAGES;
    try {
      List<PackageInfo> packageInfoList = nReactContext.getPackageManager().getInstalledPackages(flags);
      WritableArray appMaps = Arguments.createArray();

      for (PackageInfo packageInfo : packageInfoList) {
        if (!isSystemPackage(packageInfo)) {
          WritableMap appMap = Arguments.createMap();

          appMap.putString("name", packageInfo.packageName);
          appMap.putString("version", packageInfo.versionName);
          appMap.putInt("versionCode", packageInfo.versionCode);

          Date installTime = new Date(packageInfo.firstInstallTime);
          String formattedInstallTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(installTime);
          appMap.putString("firstInstallTime", formattedInstallTime);
          Date updateTime = new Date(packageInfo.lastUpdateTime);
          String formattedUpdateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(updateTime);
          appMap.putString("lastUpdateTime", formattedUpdateTime);

          appMap.putInt("targetSdkVersion", packageInfo.applicationInfo.targetSdkVersion);
          appMap.putString("appinfoName", packageInfo.applicationInfo.name);
          appMap.putString("appinfoPackageName", packageInfo.applicationInfo.packageName);
          appMap.putString("label", packageInfo.applicationInfo.loadLabel(nReactContext.getPackageManager()).toString());

          appMaps.pushMap(appMap);
        }
      }

      promise.resolve(appMaps);
    } catch (Exception e) {
      promise.reject(e.getMessage());
    }
  }

  private boolean isSystemPackage(PackageInfo packageInfo) {
    return ((packageInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0);
  }
}
