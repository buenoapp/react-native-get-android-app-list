# react-native-get-android-app-list

get complete list of install android app on a device

## Installation

```sh
npm install react-native-get-android-app-list
```

## Usage

```js
import GetAndroidAppList from "react-native-get-android-app-list";

// ...

const result = await GetAndroidAppList.getInstalledApps();
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
